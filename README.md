# Kaldi analyzer - Helena Diaz

# README

## Project Teklia. Evaluation of a Machine Learning Model.

### Main Objective:

Evaluate the performance of the Kaldi product.

This product is an open source software which allows perform automatically oral and written transcription (speech to text); to transcribe the lines of handwritten documents provided by our clients.

Specifically, the Kaldi software generate three text files with a defined structure that are the input of the package. The entire content of the files is stored in a database after a parsing proccessus, and is stored in an SQLITE3 database.

Furthermore, this data is analysed and showed in two statics reports (HTML and CSV); that contains some KPIs as Error Rate (WER), Unknown Words, Frequents Words, etc.

### Required Librarys (non natives):

For Unitary Tests:

    - pytest


For the reports:

    - pandas
    - htmlcreator
    - matplotlib

### Cloning the repository
To clone the repository, it's necessary to execute in command line:

(Via SSH)
```bash
git clone git@gitlab.com:teklia/simplon/kaldi-analyzer-helena-diaz.git 
```

(Via HTTPS)
```bash
git clone https://gitlab.com/teklia/simplon/kaldi-analyzer-helena-diaz.git
```

### Virtual Environment:
To initiate it, it's necessary to execute commands in command line:

```bash
pipenv install
pipenv shell
```

### Executing the code

The repository has an executable package called **kaldi_utt**.
Once cloned and been into the package (..kaldi_utt/), it's possible to generate the parsing and creation the DB thought the command:

```bash
python3 parser/kaldi-utt-parse.py <path_to_folder_where_are_the_input_files_stored>
```

And for generate the report, being in the same directory (..kaldi_utt/):

```bash
python3 parser/kaldi-utt-report.py <path_to_folder_where_is_the_stop_words_file>
```

### Unitary Tests

To execute the tests, it's necessary to be in the main directory (..kaldi_utt/):

```python
pytest -v tests
```
### Database in SQLITE3

It's named **kaldi_db.db**, and it's generated after executing the parser command.
It's stored in the the directory:

__..kaldi_utt/parser/kaldi_db.db__


### Reports

The executable genere two reports, in format HTML et CSV, that are stored in the path:

__..kaldi_utt/parser/report/html_report.html__

__..kaldi_utt/parser/report/csv_report.html__

### Actual Structure:

* kaldi_utt/ 
    * \__init__.py
    * parser/
        * kaldi_db.db
        * kaldi-utt-parse.py
        * kaldi-utt-report.py
        * create_dbs.py
        * list_creator.py
        * report_html.py
        * report_csv.py
        * \__init__.py
        * report/
            * html_report.html
            * csv_report.cvs

    * tests/
        * test_createdb.py
        * test_dataset.py
        * test_extract.py
        * test_table_lines.py
        * test_token_list.py
        * test_transpose.py
        * \__init__.py

* Pipfile
* Pipfile.lock
* README

The Database kaldi_db contains the tables:
- LINES: store the _reference of the lines_ and the _operator count_.
- TOKEN: store the _operator_ for evevry word.
- DATASET: store the _reference_ of the input files.




import os
import argparse
import report_csv
import report_html


if os.path.exists('parser/csv_report.csv'):
    os.remove('parser/csv_report.csv')
    
if not os.path.exists('parser/report/'):
    os.makedirs('parser/report/')
       
rep_html = report_html.html_creator()

rep_csv = report_csv.csv_creator()

print('End kaldi-utt-report.py')

import csv
import re
import itertools as it
import sys
import os
import argparse


def aggrparses():
    '''
    This function takes the path indicated by the user 
    where are stored the input files to analyse.
    '''
    
    parser = argparse.ArgumentParser()
    parser.add_argument("data", type=str, help="It takes as argument the path of the input data")
    
    args = parser.parse_args()

    if args.data:
        input_files = os.listdir(args.data)
        return input_files, args.data

    else:
        return 'Invalid argument (probably wrong path)'


def extract_text(input_files, path):
    '''
    This function takes the input documents and put them into a list in the order:
    line number id(idx), work identity (whole_ref), operation name (op), sentence,
    quantity of words per sentence (len_sent) and file name.
    '''
 
    op = []
    whole_ref = []
    sentences = []
    file_name = []

    i = 0
    for in_files in input_files:
        with open(f"{path}/{in_files}", 'r') as data:               
                for count, val in enumerate(data, start=0):
                    m = re.search(r'([^\s]+) ([^\s]*) ([ ^ ].*|[^ ].*)', val)
                    if (m is not None) and (m.group(2) in ['ref', 'hyp', 'op', '#csid']):
                        whole_ref.append(m.group(1))
                        op.append(m.group(2))
                        sentences.append(m.group(3))
                        file_name.append(in_files)
                    else:
                        raise ValueError
                        
        extract_t = list(zip(whole_ref,op, sentences, file_name))
    return extract_t


def transpose_columns(extracted_):
    '''
    This function split and transpose the lines of the input files.
    Dependencies:
        - extract_text().
    Returns a list with:
        - whole_ref: line reference
        - sentences: words per line
       - work_name: first element of the split('/') operation applied to the whole_ref 
         (Example: whole_ref = 'iam/m01/m01- 049/m01-049-05'; work_name = 'iam')
       - file_name
       - dupl_ind: indicates if it's a duplicated line
       - idx: line id
    '''
    
    if len(extracted_) % 4 == 0:
    # To check that the doc has groups of 4 lines    
        
        reorder_ = []
        for ex in extracted_:
            if len(ex) == 4:
                # To check that groups has 4 elements
                reorder_.append(ex[2])
            else: 
                raise ValueError

        if reorder_:
            whole_ref = []
            sentences = []
            file_name = []
            work_name = []
            ind_dupl = []
            for n in range(0,len(extracted_),4):
                whole_ref.append(extracted_[n][0])
                if 'duplicated' in extracted_[n][0]:
                    ind_dupl.append(True)
                else:
                    ind_dupl.append(False)
                sentences.append(reorder_[n:n+4])
                file_name.append(extracted_[n][3])
                work_name.append(extracted_[n][0].split('/')[0])
            idx = []
            for n in range(len(whole_ref)):
                idx.append(n+1)
            transpose = [whole_ref, sentences, work_name, file_name, ind_dupl, idx]
            
            return transpose
    else: 
        raise ValueError


def table_lines(trans_col, data_set):
    '''
    Function to order the data to create the table lines
    Dependencies:
        - transpose_columns()
        - table_dataset()
    Returns a list with:
        - line_id
        - whole_ref
        - csid
        - ind_dupl
        - dataset_id
    '''
    tab_lines = []
    for a in range(len(trans_col[0])):
        if len(trans_col) == 6:
            for dset in data_set:
                if dset[1] in trans_col[3][a]:
                    tab_lines.append((trans_col[5][a], trans_col[0][a], trans_col[1][a][3], trans_col[4][a], dset[0]))
        else: 
            raise ValueError
    return tab_lines


def table_token(arg):
    '''
    This convert the extract_text into the token_list
    Dependencies:
        - transpose_columns()
    Returns a list with:
        - word_ref
        - word_pos
        - word_pred
        - op_w
        - lines_id
    '''

    token_list=[]
    for tok in range(len(arg[0])):
        word_ref = arg[1][tok][0].split()
        len_lr = len(word_ref)
        word_pos = list(range(1,len_lr+1))
        word_pred = arg[1][tok][1].split()
        op_w = arg[1][tok][2].split()
        if (len(arg) == 6) and (len(op_w) == max(word_pos)):
            lines_id = len_lr*[arg[5][tok]]
            token_list.append(list(map(list, zip(*(word_ref, word_pos, word_pred, op_w, lines_id)))))
        else:
            raise ValueError
  
    tok_list = it.chain.from_iterable(token_list)
    return list(tok_list)


def table_dataset(arg):
    '''
    Function to create the list for the dataset table
    Dependencies:
        - extract_text()
    Returns a list with:
        - ds_id
        - type ('val', 'test'; 'train')
        - file_name
        - work_name
    '''

    work_name =  []
    file_name = []
    type_ = []
    ds_id = []
    for lines in (arg):
        if (len(lines) == 4) and ('per_utt' in lines[3]):
            work_name.append(lines[0].split("/")[0])
            file_name.append(lines[3])
            len_sfilename = len(lines[3].split("_"))
            type_.append(lines[3].split("_")[len_sfilename-1].split(".")[0])   
            if 'train' in lines[3]:
                ds_id.append(1)
            if 'test' in lines[3]:
                ds_id.append(2)
            if 'val' in lines[3]:
                ds_id.append(3)
        else:
            raise ValueError

    table_ds = list(zip(ds_id, type_,file_name,work_name))
    
    return sorted(set(table_ds))
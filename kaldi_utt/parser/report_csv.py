import sql_querys
import pandas as pd
import csv

def csv_creator():
    '''
    Creates an csv report of the listed functions:
    - current_words()
    - current_words_no_symbols_total(list_link)
    - current_words_no_symbols(list_link)
    - commun_errors(list_link)
    - wer_per_line_total()
    - wer_per_file()
    - total_unknown_words()
    - list_not_known_words()    
    '''
    
    list_link = sql_querys.call_link_words(sql_querys.aggrparses()[0][0], sql_querys.aggrparses()[1])

    list_of_dfs = [sql_querys.current_words()[2],sql_querys.current_words_no_symbols_total(list_link),
        sql_querys.current_words_no_symbols(list_link), sql_querys.commun_errors(list_link),
        sql_querys.wer_per_line_total(), sql_querys.wer_per_file(),sql_querys.total_unknown_words(), 
        sql_querys.list_not_known_words()]
    
    list_of_functions =['Function : current_words()',
                        'Function : current_words_no_symbols_total(list_link)', 
                        'Function : current_words_no_symbols(list_link)', 
                        'Function : commun_errors(list_link)', 
                        'Function : wer_per_line_total()', 
                        'Function : wer_per_file()',
                        'Function : total_unknown_words()', 
                        'Function : list_not_known_words()']

    for i, df in enumerate(list_of_dfs):
        with open('parser/report/csv_report.csv','a') as f: 
            f.write(list_of_functions[i])
            f.write("\n")
            f.write('_________________________________________________\n')
            df.to_csv(f)
            f.write("\n")
    f.close()
           
       


import lists_creator
import sqlite3
import csv
import argparse
import os
import pandas as pd

def aggrparses():
    '''
    This function takes the path to the 'linking words document',
    indicated by the user, and returns the path and the name of the file.
    '''
    
    parser = argparse.ArgumentParser()
    parser.add_argument("data", type=str, help="-It takes as argument the path of the input data (ex: '../data')-")

    args = parser.parse_args()

    if args.data:
        input_files = os.listdir(args.data)
        return input_files, args.data

    else:
        return 'Invalid argument (probably wrong path)'

def call_link_words(input_files, path):
    '''
    This function takes the text file with the 
    linking words defined by the user, stored in the 'linking_words' document
    Dependencies:
        - aggrparses()
    '''
    
    if input_files is None:
        input_files = aggrparses()[0]
    
    if path is None:
        path = aggrparses()[1]
    
    with open(f"{path}/{input_files}", 'r') as links:        
        numlink = links.readlines()
        l_links = []
        for l in numlink:
            l = l.rstrip("\n").replace(' ','')
            l_links.append(l)

    return l_links


# # General information

def general_info():
    '''
    Returns a DataFrame that contains general information 
    of the file(s) analyzed, obtained from the dataset table.
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    general_info = pd.read_sql_query('''select file_name, type from dataset;''',conn)
    
    conn.commit()
    
    conn.close()    

    return general_info


# # Question number 1

def current_words():
    '''
    This functions makes the SQL queries related to most currents words and resume the total words.
    Returned values: 
        - totalt : Total of words_ref in all files
        - totald : Total of DISCTINCT words_ref in all files
        - current_nsl : Top 10 most current words
    
    '''
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    totalt = conn.execute('''select count(word_ref) from token;''').fetchall()
    
    totald = conn.execute('''select count(distinct(word_ref)) from token;''').fetchall()

    current_nsl = pd.read_sql_query('''select count(word_ref) as count, 
    word_ref from token group by word_ref order by count DESC limit 15;''',conn)
    
    conn.commit()
    
    conn.close()    

    return totalt[0][0], totald[0][0], current_nsl


def current_words_no_symbols_total(list_link):
    '''
    This functions makes the SQL queries related to most currents words 
    filtering the words declared in the 'linking_words' document.
    Dependencies:
        - call_link_words()
    Returned values:
        - current_ns : DataFrame with the Top 10 most current words without linking words
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    if list_link is None:
        list_link = call_link_words()
    
    comparative_list = '('+'"'+'","'.join(list_link)+'"'+')'

    current_ns = pd.read_sql_query(f'''select count(word_ref) as count, word_ref from token 
    where word_ref not in {comparative_list}
    group by word_ref 
    order by count DESC limit 15''', conn)

    conn.commit()
    conn.close()

    return current_ns


def current_words_no_symbols(list_link):
    '''
    This function returns a DataFrame with the first 10 more common words
    and group them by type of file: 'test', 'train' or 'val'
    Dependencies:
        - call_link_words()
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
      
    if list_link is None:
        list_link = call_link_words()   
    
    comparative_list = '('+'"'+'","'.join(list_link)+'"'+')'

    current_ns_types = pd.read_sql_query(f'''select count(token.word_ref) as count ,
    token.word_ref, ds.type
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where token.word_ref not in {comparative_list} 
    group by token.word_ref order by count DESC;''', conn)

    conn.commit()
    conn.close()

    return current_ns_types


def commun_errors(list_link):
    '''
    This functions returns a DataFrame with the 15 most common errors, per type of file.
    Dependencies:
        - call_link_words()
    '''
    conn = sqlite3.connect('parser/kaldi_db.db')
     
    if list_link is None:
        list_link = call_link_words()   

    comparative_list = '('+'"'+'","'.join(list_link)+'"'+')'
    
    commun_error = pd.read_sql_query(f'''select word_ref, word_pred, count(op_w) as total from token 
    where op_w != 'C' and word_ref not in {comparative_list} 
    and word_pred not in {comparative_list} 
    group by op_w, word_ref order by total DESC limit 15;''', conn)
    
    conn.commit()
    conn.close()

    return commun_error


def wer_per_line_total():
    '''
    This function calcules the wer per line as the formula: WER = (S + D + I) / (S + D + C) * 100.
    Returns a DataFrame with the values of:
        - Error rate (wer)
        - numerator
        - denominator
        - line_id
        - type ('val', 'test' or 'train')
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    wer_ = pd.read_sql_query('''
    select 
    case
    when round(100*CAST(num AS REAL)/den,2) not NULL then round(100*CAST(num AS REAL)/den,2)
    else 0
    end WER, num, den, line_den as line, type from
    (
    select sum(ind_den) as den, type ,line as line_den from (
    select token.op_w as op, ds.type as type, token.lines_id as line,
    case 
    when token.op_w in ('S', 'D', 'C') then 1
    else 0
    end ind_den
    from token
    join lines on lines.id = token.lines_id
    join dataset as ds on lines.dataset_id = ds.id) as inner2
    group by line_den
    )
    JOIN
    (
    select sum(ind_num) as num, line as line_num from (
    select token.op_w as op, ds.type as type, token.lines_id as line,
    case 
    when token.op_w in ('S', 'D', 'I') then 1
    else 0
    end ind_num 
    from token
    join lines on lines.id = token.lines_id
    join dataset as ds on lines.dataset_id = ds.id) as inner1
    group by line_num
    )
    ON line_num = line_den
    order by WER DESC;
    ''', conn)

    conn.commit()
    conn.close()

    return wer_


def wer_per_file():
    '''
    This function calcules the wer per file as the formula: WER = (S + D + I) / (S + D + C) * 100
    Returns a DataFrame with the values of:
        - Error rate (wer)
        - numerator
        - denominator
        - line_id
        - type ('val', 'test' or 'train')
    '''
        
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    types_ = conn.execute('''select distinct(type) as type from dataset;''').fetchall()
    
    wer_ = pd.read_sql_query('''select 
    case
    when round(100*CAST(num AS REAL)/den,2) not NULL then round(100*CAST(num AS REAL)/den,2)
    else 0
    end WER, num, den, type_den as type from
    (
    select sum(ind_den) as den, type_den from (
    select token.op_w as op, ds.type as type_den,
    case 
    when token.op_w in ('S', 'D', 'C') then 1
    else 0
    end ind_den
    from token
    join lines on lines.id = token.lines_id
    join dataset as ds on lines.dataset_id = ds.id
    ) as inner2
    group by type_den
    )
    JOIN
    (
    select sum(ind_num) as num, type_num from (
    select token.op_w as op, ds.type as type_num,
    case 
    when token.op_w in ('S', 'D', 'I') then 1
    else 0
    end ind_num
    from token
    join lines on lines.id = token.lines_id
    join dataset as ds on lines.dataset_id = ds.id
    ) as inner2
    group by type_num        
    )
    ON type_num = type_den;''', conn)

            
    conn.commit()
    conn.close()

    return wer_        
        
def total_known_words():
    '''
    This function returns the total quantity of distinct 
    known words in 'train' reference file
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    total_known_words = conn.execute('''
    select count(distinct(token.word_ref)) as total_known_words
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where ds.type = 'train';''').fetchall()
    
    conn.commit()
    conn.close()
    return total_known_words[0][0]

def total_unknown_words():
    '''
    This function returns a DataFrame with the total of 
    distinct unknown words in 'test' and 'val' file
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    total_unknown_words = pd.read_sql_query('''select count(distinct(token.word_ref)) as total_not_known_words,
    ds.type as type
    from token join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where token.word_ref NOT IN (
    select distinct(token.word_ref)
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where ds.type = 'train') 
    group by type''', conn)

    conn.commit()
    conn.close()
    return total_unknown_words


def list_known_words():
    '''
    Generate a list of the distinct known words in the 'train' file
    (Not called in the reports)
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
 
    known_words = conn.execute('''
    select distinct(token.word_ref)
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where ds.type = 'train'
    ;''').fetchall()
    
    conn.commit()
    conn.close()
    return known_words


def list_not_known_words():
    '''
    Generate a DataFrame of distinct unknown words per type of file: 'test' and 'val'
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
 
    not_known_words = pd.read_sql_query('''
    select distinct(token.word_ref) as not_known_words, ds.type as type
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where token.word_ref NOT IN (
    select distinct(token.word_ref)
    from token 
    join lines on lines.id = token.lines_id
    join dataset as ds on ds.id = lines.dataset_id
    where ds.type = 'train'
    )
    order by not_known_words DESC;''', conn)

    conn.commit()
    conn.close()

    return not_known_words



def current_words_no_symbols_eachone(list_link):
    '''
    This functions makes the SQL queries related to most currents words with no 
    symbols and linking words
    Dependencies:
        - call_link_words()
    Returned values:
        - current_ns : Top 10 most current words without linking words
    (Not called in the reports. It was intended to be used to do a 'Cloud of Words')
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    
    if list_link is None:
        list_link = call_link_words()
    
    comparative_list = '('+'"'+'","'.join(list_link)+'"'+')'

    current_ns = conn.execute(f'''select word_ref from token 
    where word_ref not in {comparative_list}''').fetchall()
   
    conn.commit()
    conn.close()
    
    cw_list = []
    for cw in current_ns:
        cw_list.append(cw[0])

    return cw_list

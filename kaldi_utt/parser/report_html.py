import sql_querys
import sqlite3
import numpy as np
import pandas as pd
from htmlcreator import HTMLDocument
import plotly.express as px

def html_creator():
    '''
    This function returns and HTML document that contains the results of the functions:
        - current_words()
        - total_known_words()
        - current_words()
        - wer_per_file()
        - wer_per_line_total()
        - current_words_no_symbols_total()
        - list_not_known_words()
        - commun_errors()
        - total_unknown_words()
    It's included an histogram of the wer_per_line_total() function, per type of file, restricted to 
    a fix range of percentage (< 80%).
    '''
    document = HTMLDocument()

    document.set_title('Kaldi static report')

    document.add_header('Main Indicators', level='h1', align='center')
    
    document.add_header('Analized Files')
    
    document.add_table(sql_querys.general_info())
    
    document.add_header('Reference Concepts')

    long_text = ['* Total of Words: Total quantity of word of the file(s) evaluated',
                '* Total of Distinct Words: Total quantity of different words of the file(s) evaluated',
                '* Total of Known Words: Total of different words found in the train file',
                '* Total of Unknown Words: Total of different words NOT found in the train file',
                '* Perc. Unknown words: percentage of unknown words in function of the Total of Distinct Words',
                '* number lines WER>0: number of lines that has at least one error',
                '* number lines Total: total number of lines of the evaluated files',
                '* Perc. WER total: percentage of error in function of the last two values'
                ]

    for n in long_text:
            document.add_paragraph(n, size='14px', indent='15px', align='justify' )

    # ______TABLES ________________________________
    document.add_header('Resume of Numbers')
    num_rows = 2
    num_cols = 8

    #list_of_sql_querys
    wpt = sql_querys.wer_per_line_total()
    wm0 = int(wpt[wpt.WER > 0].shape[0])
    wm = int(wpt.shape[0]) 

    data=[[sql_querys.current_words()[0],  sql_querys.current_words()[1], sql_querys.total_known_words(), 
           sql_querys.current_words()[1]-sql_querys.total_known_words(), 
           str(round(100*((sql_querys.current_words()[1]-sql_querys.total_known_words())/sql_querys.current_words()[1]),2))+'%',
           wm0, wm, str(round(100*(wm0/wm),2))+'%']]
    columns = ['Total of Words', 'Total of Distinct Words', 'Total of Known Words', 'Total of unknown Words',
               'Perc. Unknown words', 'number lines WER>0', 'number lines Total', 'Perc. WER total']
    df = pd.DataFrame(data, columns=columns)
    df

    document.add_table(df)
    

    ## ______GRAPHIC_1________________________________##
    document.add_header('___________ WER plots ___________')

    document.add_header('WER per file - Error percentage per document')
    document.add_table(sql_querys.wer_per_file()[['WER','type']])


    document.add_header('WER "TRAIN" file - Error percentage per line')

    conditions = ['* 0% < WER < 80%', '* x axis: WER Percentage', '* y axis: WER ocurrence']
    for cond in conditions:
        document.add_paragraph(cond, size='16px', indent='15px', align='justify' )


    wpl = sql_querys.wer_per_line_total()
    df1 = wpl[(wpl.WER > 0) & (wpl.WER < 80) & (wpl.type == 'train')]

    fig1= px.histogram(df1, x='WER',
                       color_discrete_sequence=px.colors.qualitative.Plotly[5:], 
                      opacity=0.5, nbins=10)#,

    fig1.update_layout(title={'text': 'WER for "TRAIN" file', 'x': 0.5, 'xanchor': 'center'},
                       yaxis_title="WER Occurrence",
                       xaxis_title="WER Percentage %")

    document.add_plotly_figure(fig1)

    df1_ = wpl[(wpl.WER > 80) & (wpl.type == 'train')]
    
    document.add_header('List of lines for WER "TRAIN" file - WER > 80%')
    
    if df1_.shape[0]<10:
        document.add_table(df1_[['WER','line']])
    else:
        document.add_table(df1_[['WER','line']][:10])

    ## ______GRAPHIC_2________________________________##
    
    df2 = wpl[(wpl.WER > 0) & (wpl.WER < 80) & (wpl.type == 'val')]

    fig2= px.histogram(df1, x='WER',
                       color_discrete_sequence=px.colors.qualitative.Plotly[5:], 
                      opacity=0.5, nbins=10)#,

    fig2.update_layout(title={'text': 'WER for "VAL" file', 'x': 0.5, 'xanchor': 'center'},
                       yaxis_title="WER Occurrence",
                       xaxis_title="WER Percentage %")

    document.add_plotly_figure(fig2)
    
    document.add_header('List of lines for WER "VAL" file - WER > 80%')
    
    df2_ = wpl[(wpl.WER > 80) & (wpl.type == 'val')]
    
    if df2_.shape[0]<10:
        document.add_table(df2_[['WER','line']])
    else:
        document.add_table(df2_[['WER','line']][:10])
    
    ## ______GRAPHIC_3________________________________##
    
    df3 = wpl[(wpl.WER > 0) & (wpl.WER < 80) & (wpl.type == 'test')]

    fig3= px.histogram(df1, x='WER',
                       color_discrete_sequence=px.colors.qualitative.Plotly[5:], 
                      opacity=0.5, nbins=10)#,

    fig3.update_layout(title={'text': 'WER for "TEST" file', 'x': 0.5, 'xanchor': 'center'},
                      yaxis_title="WER Occurrence",
                      xaxis_title="WER Percentage %")

    document.add_plotly_figure(fig3)

    document.add_header('List of lines for WER "TEST" file - WER > 80%')
    
    df3_ = wpl[(wpl.WER > 80) & (wpl.type == 'test')]
            
    if df3_.shape[0]<10:
        document.add_table(df3_[['WER','line']])
    else:
        document.add_table(df3_[['WER','line']][:10])
    

    # ______TABLES ________________________________
    document.add_header('_______ CURRENT WORDS _______')

    document.add_header('15 most current words, including the symbols, from the three files together')
    document.add_table(sql_querys.current_words()[2])

    document.add_header('15 most current word, without including the symbols and the linking words')
    
    list_link = sql_querys.call_link_words(sql_querys.aggrparses()[0][0], sql_querys.aggrparses()[1])
    
    document.add_table(sql_querys.current_words_no_symbols_total(list_link))

    cwns_t = sql_querys.current_words_no_symbols(list_link)
    cwns_tr = cwns_t[(cwns_t['type']=='train')].sort_values(by='count', ascending=False)[:15]
    cwns_v = cwns_t[(cwns_t['type']=='val')].sort_values(by='count', ascending=False)[:15]
    cwns_te = cwns_t[(cwns_t['type']=='test')].sort_values(by='count', ascending=False)[:15]

    document.add_header('15 most current words from the TRAIN, without including the symbols and the linking words')
    document.add_table(cwns_tr)

    document.add_header('15 most current words from the VAL, without including the symbols and the linking words')
    document.add_table(cwns_v)

    document.add_header('15 most current words from the TEST, without including the symbols and the linking words')
    document.add_table(cwns_te)

    document.add_header('_______ COMMUN ERRORS _______')
    document.add_header('15 most wrong words (not found in the TRAIN file), without including the symbols and the linking words')
    document.add_table(sql_querys.commun_errors(list_link))


    document.add_header('_______ KNOWN AND UNKNOWN WORDS _______')

    document.add_header('Total of unknown Words')
    document.add_table(sql_querys.total_unknown_words())
    
    lnkw = sql_querys.list_not_known_words()
    lnkw_val = lnkw[lnkw.type == 'val'].sort_values(by='not_known_words', ascending=True)[:15]
    lnkw_test = lnkw[lnkw.type == 'test'].sort_values(by='not_known_words', ascending=True)[:15]

    document.add_header('First 15 words unknown, alphabetically ordered, file: "VAL"')
    document.add_table(lnkw_val)

    document.add_header('First 15 words unknown, alphabetically ordered, file: "TEST"')
    document.add_table(lnkw_test)


    # Write to file
    output_filepath = 'parser/report/html_report.html'
    document.write(output_filepath)
    return document.write(output_filepath)




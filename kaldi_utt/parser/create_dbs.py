
try:
    import lists_creator
except ImportError:
    from kaldi_utt.parser import lists_creator
    
import sqlite3

def create_table_lines(lin_,conn):
    '''
    Calls the table_lines() function to create the 
    table 'lines' in the db kald_db.db
    '''
    
    if lin_ is None:
        ds_ = lists_creator.dataset_table(lists_creator.extract_text(lists_creator.aggrparses()[0], lists_creator.aggrparses()[1]))
        lin_ = lists_creator.table_lines(lists_creator.transpose_columns(lists_creator.extract_text(lists_creator.aggrparses()[0],lists_creator.aggrparses()[1])),ds_)
    
    if conn is None:
        conn = sqlite3.connect('parser/kaldi_db.db')
        
    print ("Opened database successfully")


    conn.execute('''CREATE TABLE IF NOT EXISTS lines
             (id INTEGER PRIMARY KEY,
              whole_ref TEXT,
              csid TEXT,
              ind_dupl BOOL,
              dataset_id INTEGER,
              FOREIGN KEY (dataset_id) REFERENCES dataset (id)
                  ON DELETE CASCADE,
              UNIQUE (id, whole_ref, csid, ind_dupl, dataset_id) ON CONFLICT IGNORE)
              ;''')

    conn.executemany('INSERT INTO lines (id, whole_ref, csid, ind_dupl, dataset_id) VALUES (?,?,?,?,?)', lin_)

    print ("Create and filled table 'lines' successfully")
        
    conn.commit()


def create_table_token(tk_, conn):
    '''
    Calls the table_token() function to create the 
    table 'token' in the db kald_db.db
    '''
    
    if tk_ is None:
        tk_ = lists_creator.table_token(lists_creator.transpose_columns(lists_creator.extract_text(lists_creator.aggrparses()[0],lists_creator.aggrparses()[1])))
    
    if conn is None:
        conn = sqlite3.connect('parser/kaldi_db.db')

    conn.execute('''CREATE TABLE IF NOT EXISTS token
             (word_ref TEXT,
              word_pos TEXT,
              word_pred TEXT,
              op_w TEXT,
              lines_id INTEGER,
              FOREIGN KEY (lines_id) REFERENCES lines (id)
                  ON DELETE CASCADE,
              UNIQUE (word_ref, word_pos, word_pred, op_w, lines_id) ON CONFLICT IGNORE)
              ;''')

    
    conn.executemany('INSERT INTO token (word_ref, word_pos, word_pred, op_w, lines_id) VALUES (?,?,?,?,?)', tk_)

    print ("Created and filled table 'token' successfully")

    conn.commit()


def create_table_dataset(ds_,conn): 
    '''
    Calls the table_dataset() function to create the 
    table 'dataset' in the db kald_db.db
    '''
    
    if ds_ is None:
        ds_ = lists_creator.table_dataset(lists_creator.extract_text(lists_creator.aggrparses()[0], lists_creator.aggrparses()[1]))
        
    if conn is None:
        conn = sqlite3.connect('parser/kaldi_db.db')
    
    try:
    
        conn.execute('''CREATE TABLE IF NOT EXISTS dataset
                 (id INTEGER PRIMARY KEY,
                 type TEXT,
                 file_name TEXT,
                 work_name TEXT,
                 UNIQUE (id, type,file_name, work_name) ON CONFLICT IGNORE)
                 ;''')

        conn.executemany('INSERT INTO dataset (id, type,file_name, work_name) VALUES (?,?,?,?)', ds_) 

        print ("Created and filled table 'dataset' successfully")
        conn.commit()
    
    except sqlite3.IntegrityError:
            print ('KeyConflict() _ There are documents from different works in the given path: ',lists_creator.aggrparses()[1],'. The database was not created.')

def close_cursor():
    '''
    Close the connection with the sqlite3 db
    '''
    
    conn = sqlite3.connect('parser/kaldi_db.db')
    conn.close()

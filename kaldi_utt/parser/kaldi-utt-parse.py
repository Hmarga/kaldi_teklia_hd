import create_dbs
import lists_creator
import argparse
import sqlite3
import os
import pytest


if os.path.exists('parser/kaldi_db.db'):
    os.remove('parser/kaldi_db.db')

arg_console = lists_creator.aggrparses()


### Call to parsers functions, used as variables in the main.py
extracted_fcn = lists_creator.extract_text(arg_console[0], arg_console[1])

transpose_fcn = lists_creator.transpose_columns(extracted_fcn)

dataset_fcn = lists_creator.table_dataset(extracted_fcn)

lines_fcn = lists_creator.table_lines(transpose_fcn, dataset_fcn)

token_fcn = lists_creator.table_token(transpose_fcn)


### Call to tables creators functions
conn_= sqlite3.connect('parser/kaldi_db.db')

cr_dbtable = create_dbs.create_table_lines(lines_fcn, conn_)

cr_dbtoken = create_dbs.create_table_token(token_fcn, conn_)

cr_dbdataset = create_dbs.create_table_dataset(dataset_fcn, conn_)

cr_closecursor = create_dbs.close_cursor()

print('End kaldi-utt-paser.py')
# #cr_report = create_report.print_report()

import pytest
import sys
import os

from kaldi_utt.parser import lists_creator



def test_extract_text_success():
    '''
    It creates a test file in the parser/ folder ('...parser/') to check the success of the extract_text() function: 
        - That the file name contains the same quantity of lines that the function returns.
        - That the file is in the correspondent folder.
    '''
    with open('../test_extract_succ.txt', 'w') as f:
        f.write('coeur/25 ref  a , b \ncoeur/25 hyp  a , c \ncoeur/25 op   C C S \ncoeur/25 #csid 2 1 0 0')

    with open("../test_extract_succ.txt") as file:
        numline = len(file.readlines())
        path = ".."
        
        assert (len(lists_creator.extract_text(['test_extract_succ.txt'],path)) == numline)

        assert (lists_creator.extract_text(['test_extract_succ.txt'], path) == [('coeur/25', 'ref', ' a , b ', 'test_extract_succ.txt'), ('coeur/25', 'hyp', ' a , c ', 'test_extract_succ.txt'), ('coeur/25', 'op', '  C C S ', 'test_extract_succ.txt'), ('coeur/25', '#csid', '2 1 0 0', 'test_extract_succ.txt')])
    os.remove("../test_extract_succ.txt")

def test_extract_text_failure():
    '''
    Test the failure of the extract_text() function.
    '''
    
    with open("../test_extract_fail.txt", "w") as f:
        f.write('coeur_25 a')
    path = ".."
    with pytest.raises(ValueError):
        lists_creator.extract_text(['test_extract_fail.txt'], path)

    os.remove("../test_extract_fail.txt")

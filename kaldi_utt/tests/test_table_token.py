import pytest
from kaldi_utt.parser import lists_creator


'''
It creates a test file in the modules/ folder (...modules/) to check: 
    - That the file name contains the same quantity of lines that the exit of the function
    - That the file is in the correspondent folder
'''

def test_token_list_success():
    '''
    Test the success of the table_token() function.
    '''
    
    r_example =[['coeur/01'],[["house , dog def , g","house , dog def , g", " C C C C C C ", "6 0 0 0"]],
    ['coeur'], ['coeur_test.txt'], [True], [1]]
    
    
    assert (lists_creator.table_token(r_example)[0] == ['house', 1, 'house', 'C', 1])

def test_token_list_failure():
    '''
    Test the failure of the table_token() function.
    '''
        
    w_example = [['coeur/01'],[["house , dog def , g","house , dog def , g", " C C C C C ", "6 0 0 0"]],
    [2], ['coeur'], ['coeur_test.txt'], [True], ['test']]
    
    with pytest.raises(ValueError):
         lists_creator.table_token(w_example)
           
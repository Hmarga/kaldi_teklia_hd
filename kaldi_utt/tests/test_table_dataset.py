import pytest
from kaldi_utt.parser import lists_creator

'''
It creates a test file in the modules/ folder (...modules/) to check: 
    - That the file name contains the same quantity of lines that the exit of the function
    - That the file is in the correspondent folder
'''

def test_dataset_success():
    '''
    Test the success of the table_dataset() function.
    '''
    r_example = [('coeur/01', 'ref', "house , dog def , g", 'coeur_per_utt_test.txt'),
 ('coeur/02', 'ref', "house , dog def , g", 'coeur_per_utt_val.txt'),
 ('coeur/03', 'ref', "house , dog def , g", 'coeur_per_utt_train.txt')]
    
    
    assert (lists_creator.table_dataset(r_example)[0] == (1, 'train', 'coeur_per_utt_train.txt', 'coeur'))
    
def test_dataset_failure():
    '''
    Test the failure of the table_dataset() function.
    '''
    
    w_example = [('coeur/01', 'ref', "house , dog def , g", 'coeur_per_utt_test.txt'),
 ('coeur/02', 'ref', "house , dog def , g", 'coeur_per_utt_val.txt'),
 ('coeur/03', 'ref', "house , dog def , g", 'coeur_utt_train.txt')]
    
    with pytest.raises(ValueError):
         lists_creator.table_dataset(w_example)
            
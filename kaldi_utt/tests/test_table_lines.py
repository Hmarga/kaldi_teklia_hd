import pytest
from kaldi_utt.parser import lists_creator


'''
It creates a test file in the parser/ folder (... parser/) to check: 
    - That the file name contains the same quantity of lines that the exit of the function
    - That the file is in the correspondent folder
'''


def test_table_lines_success():
    '''
    Test the success of the table_lines() function.
    '''
        
    ds_ = [(1, 'train', 'coeur_train.txt', 'coeur'),
         (2, 'test', 'coeur_test.txt', 'coeur'),
         (3, 'val', 'coeur_val.txt', 'coeur')]
    
    r_example =[['coeur/01'],[["a , b c def , g","a , b c def , g", " C C C C C C C ", "7 0 0 0"]],
        ['coeur'],
        ['coeur_test.txt'],
        [False],
        [1]]
        

    assert (len(lists_creator.table_lines(r_example ,ds_)[0]) == 5)
        
    assert (lists_creator.table_lines(r_example, ds_)[0][1] == 'coeur/01')
    
def test_table_lines_failure():
    '''
    Test the failure of the table_lines() function.
    '''
    
    ds_ = [(1, 'train', 'coeur_train.txt', 'coeur'),
         (2, 'test', 'coeur_test.txt', 'coeur'),
         (3, 'val', 'coeur_val.txt', 'coeur')]
    
    w_example =[['coeur/01'],
       [["a , b c def , g","a , b c def , g", " C C C C C C C ", "7 0 0 0"]],
        ['coeur'],
        ['coeur_test.txt'],
        [False]]
    
    with pytest.raises(ValueError):
         lists_creator.table_lines(w_example, ds_)
            

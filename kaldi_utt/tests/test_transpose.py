import pytest
from kaldi_utt.parser import lists_creator


def test_transpose_columns_success():
    '''
    Test the failure of the transpose_columns() function.
    '''
    right_input = [('coeur/25', 'ref', ' that is a good one ','coeur_test.txt'),
     ('coeur/25', 'hyp', ' that is o good one ','coeur_test.txt'),
     ('coeur/25', 'op', ' C C S C C ','coeur_test.txt'),
     ('coeur/25', '#csid', ' 4 1 0 0 ','coeur_test.txt')]

    right_result = [['coeur/25'], [[' that is a good one ', ' that is o good one ', ' C C S C C ', ' 4 1 0 0 ']], ['coeur'], ['coeur_test.txt'], [False], [1]]

    assert (lists_creator.transpose_columns(right_input) == right_result)


def test_transpose_columns_failure():
    '''
    Test the failure of the transpose_columns() function.
    '''
    
    wrong_input = [('coeur/25', 'ref', ' that is a good one ','coeur_test.txt'), ('coeur/25', 'hyp', ' that is o good one ','coeur_test.txt'), ('coeur/25', 'op', ' C C S C C ','coeur_test.txt')]
    with pytest.raises(ValueError):
        lists_creator.transpose_columns(wrong_input)
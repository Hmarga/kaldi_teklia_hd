
import os
import pathlib
import sqlite3
import pytest
from kaldi_utt.parser import create_dbs
from kaldi_utt.parser import lists_creator
    
@pytest.fixture(scope="function")
def db():
    conn = sqlite3.connect(":memory:")
    yield conn
    conn.close()
    
def test_create_table_dataset_success(db):
    '''
    Test the success of the create_table_dataset() function, counting the quantity of lines and the table content.
    '''
    
    # Action
    ds_test = [(1, 'train', 'coeur_per_utt_train.txt', 'coeur'), (2, 'test', 'coeur_per_utt_test.txt', 'coeur'), (3, 'val', 'coeur_per_utt_val.txt', 'coeur')]

    create_dbs.create_table_dataset(ds_test, db)
    
    # Validations
    cur = db.cursor()
    cur.execute("SELECT * FROM dataset")
    rows = cur.fetchall()
    assert (len(rows) == 3)
    
    cur = db.cursor()
    cur.execute("select distinct(type) from dataset")
    rows = cur.fetchall()
    for n in rows: 
        assert (n[0] in ['train', 'test', 'val'])
        
    cur = db.cursor()
    cur.execute("select distinct(id) from dataset")
    rows = cur.fetchall()
    for n in rows: 
        assert (n[0] in [1, 2, 3])
    
def test_create_table_token_success(db):
    '''
    Test the success of the create_table_token() function, counting the length and content of some rows.
    '''
        
    #Test token table#
    tk_test = [['house', 1, 'house', 'C', 1],
 [',', 2, ',', 'C', 1],
 ['dog', 3, 'doge', 'S', 1],
 ['def', 4, 'def', 'C', 1],
 [',', 5, ',', 'C', 1],
 ['g', 6, 'g', 'C', 1],]   
    
    create_dbs.create_table_token(tk_test, db)

    cur = db.cursor()
    cur.execute("SELECT * FROM token")
    rows = cur.fetchall()
    assert len(rows) == 6
    
    cur = db.cursor()
    cur.execute("select distinct(lines_id) from token")
    rows = cur.fetchall()
    assert rows[0] == (1,)
    
    
def test_create_table_lines_success(db):
    '''
    Test the success of the create_table_lines() function, counting the length and content of some rows.
    '''
    
    lines_test = [(50, 'coeur/01', '10 3 0 0', False, 2),
 (51, 'coeur/02', '9 0 0 0', False, 2),
 (52, 'coeur/03', '6 3 0 0', False, 2),
 (53, 'coeur/04', '7 2 1 0', False, 2)]
    
    create_dbs.create_table_lines(lines_test, db)

    cur = db.cursor()
    cur.execute("SELECT * FROM lines")
    rows = cur.fetchall()
    assert len(rows) == 4
    
    
    cur = db.cursor()
    cur.execute("select distinct(ind_dupl) from lines group by ind_dupl")
    rows = cur.fetchall()
    for n in rows: 
        assert (n[0] in [0, 1])

